/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet, Nhi Pham */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var slideshow = [0], /* map slide number to cached slides; first always 0 */
    slideshowNum = 0,  /* slideshow identifier for cache mechanism */
    slideNum = 0, /* current slide */
    numberOfSlides = 0,
    fetchSlideNum = null, /* current slide to be cached */
    isCached = [true]; /* tells if slide has been cached; first always true */

var socket = io({ auth: { token: "@lecturer-key" } });

/***  mechanism for displaying and silently caching slides  ***/

function slideFromNum(num) {
    return "slides/slide_" + slideshowNum + "_" + slideshow[num] + ".svg";
};

function cachedSlide(){
    isCached[slideshow[fetchSlideNum]] = true;
    fetchNextSlide();
}

function fetchNextSlide(){
    fetchSlideNum = (fetchSlideNum % numberOfSlides) + 1;
    if (fetchSlideNum === slideNum){ /* circular run of the slideshow */
        document.getElementById("slide").removeEventListener("load",
            cachedSlide);
        fetchSlideNum = null;
    }else if (isCached[slideshow[fetchSlideNum]]){
        fetchNextSlide();
    }else{
        document.getElementById("cache").addEventListener("load",
            cachedSlide, { once: true });
        document.getElementById("cache").src = slideFromNum(fetchSlideNum);
    }
};

/* set new slideshow, go to current slide and start caching if necessary */
var newSlideshow = socket.on("newSlideshow", function(msg){
    let newNumberOfSlides = msg.slideshow.length - 1;
    let numberOfNewSlides = newNumberOfSlides - numberOfSlides;

    slideshow = msg.slideshow;
    slideshowNum = msg.slideshowNum;
    slideNum = msg.slideNum;
    numberOfSlides = newNumberOfSlides;

    if (numberOfNewSlides > 1){ /* initialize caching mechanism */
        fetchSlideNum = slideNum;
        isCached.push(...Array(numberOfNewSlides).fill(false));
        document.getElementById("slide").addEventListener("load", cachedSlide);
    }

    document.getElementById("slide").src = slideFromNum(slideNum);
});

/* navigate to another slide */
var goToSlide = socket.on("goToSlide", function(num) {
    slideNum = num;
    if (fetchSlideNum){
        /* "slide" is already supposed to trigger cachedSlide() */
        document.getElementById("cache").removeEventListener("load",
                cachedSlide);
        fetchSlideNum = slideNum; // ensure next fetched slide is next slide
    };
    document.getElementById("slide").src = slideFromNum(slideNum);
});

/* change background color */
var backgroundColor = socket.on("backgroundColor", function(color){
    document.body.style.backgroundColor = color;
    /* lecturer start */
    document.getElementById("chooseBackgroundColor").value = color;
    /* lecturer end */
    /* set foreground color according to perceived brightness */
    let RGB = color.match(/[A-Za-z0-9]{2}/g).map(v => parseInt(v, 16));
    let brightness = 0.299*RGB[0] + 0.587*RGB[1] + 0.114*RGB[2];
    if (brightness < 255/2){
        document.body.style.color = "rgb(255, 255, 255)";
    }else{
        document.body.style.color = "rgb(0, 0, 0)";
    }
});

/* lecturer start */

/***  beamer navigation  ***/

document.addEventListener("keydown", function(e){
    switch (e.keyCode) {
    case 37: // left
        socket.emit("incrSlide", -1);
        break;
    case 40: // down
        socket.emit("incrSlide", 10);
        break;
    case 38: // up
        socket.emit("incrSlide", -10);
        break;
    case 39: // right
        socket.emit("incrSlide", 1);
        break;
    case 33: // page up
        socket.emit("prevSlide");
        break;
    case 34: // page up
        socket.emit("lastSlide");
        break;
    }
}, false);

/***  buttons for modifying beamer slides  ***/

document.getElementById("chooseBackgroundColor").addEventListener("change",
    function(e){ socket.emit("backgroundColor", e.target.value); }
);

document.getElementById("add-blank-slide").addEventListener("click", () => {
    socket.emit("addBlankSlide");
}, false);

document.getElementById('add-slides').addEventListener('click', (e) => {
    document.getElementById("input-add-slides").click();
}, false);

document.getElementById("clean-slide").addEventListener("click", () => {
    socket.emit("cleanSlide");
}, false);

document.getElementById("clean-all-slides").addEventListener("click", () => {
    socket.emit("cleanAllSlides");
}, false);

document.getElementById("cut-slide").addEventListener("click", () => {
    socket.emit("cutSlide");
}, false);

document.getElementById("cut-all-slides").addEventListener("click", () => {
    socket.emit("cutAllSlides");
}, false);

/***  uploading slide mechanism  ***/

var uploadingFileInfo;

document.getElementById("input-add-slides").addEventListener("change", (e) => {
    uploadingFileInfo = e.srcElement.files[0];
    if (!uploadingFileInfo || !uploadFileValidation()){
        uploadingFileInfo = null;
        return;
    }
    socket.emit("startUploadSlide", {
        filename: uploadingFileInfo.name.replace(/\s/g, "_"), 
        type: uploadingFileInfo.type
    });
}, false);

var startUploadSlide = socket.on("startUploadSlide", function(){
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState == 4 && xhr.status == 202){
            uploadNotification.show("Fichier téléversé avec succès. " +
                "Veuillez patienter pendant la conversion.", "", 90000);
        }
    }
    xhr.open('POST', '/@lecturer-key/beamer/beamer.html', true);
    xhr.overrideMimeType('application/octet-stream; ' +
        'charset=x-user-defined-binary');    
    let reader = new FileReader();
    reader.onload = function(e){
        uploadNotification.show("Veuiller patienter pendant le téléversement "
            + "du fichier.", "", 90000);
        xhr.send(reader.result);
    }
    reader.readAsArrayBuffer(uploadingFileInfo);
})

var stopUploadSlide = socket.on("stopUploadSlide", function(msg){
    alert(msg);
});

var endUploadSlide = socket.on("endUploadSlide", function(){
    uploadNotification.show("Diapositive(s) ajoutée(s) avec succès.",
        "success", 3000);
    document.getElementById("input-add-slides").value = [];
});

function uploadFileValidation() {
    /* check valid file types */
    let type = uploadingFileInfo.type;
    if (!(type === "application/pdf" || type.split("/")[0] === "image")){
        alert("Pour téléverser des diapositives, les seuls formats acceptés " +
            "sont les images ou PDF.");
        return false;
    }
    if (uploadingFileInfo.size >= server_config_beamer.MAX_FILE_SIZE){ 
        alert(
            "Fichier trop gros pour téléversement ; " +
            "taille maximale autorisée : " +
            server_config_beamer.MAX_FILE_SIZE / (1024*1024) + " MiB."
        );
        return false;
    }
    return true;
}

const uploadNotification = {
    init(){
        this.hideTimeout = null;
        this.el = document.createElement("div");
        document.body.appendChild(this.el);
    },
    show(message, state, time){
        clearTimeout(this.hideTimeout);
        this.el.textContent = message;
        this.el.className = "toast toast--visible";
        if (state){
            this.el.classList.add(`toast--${state}`);
        }
        this.hideTimeout = setTimeout(()=>{
            this.el.classList.remove('toast--visible');
        }, time);
    }
};

document.addEventListener("DOMContentLoaded", function(){
    uploadNotification.init();
});

/* lecturer end */
