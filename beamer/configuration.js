const server_config_beamer = {
    CACHE_MAX_AGE: 4800, // 1 h 20 min
    MAX_FILE_SIZE: 1024 * 1024 * 4 //4 MiB
};
