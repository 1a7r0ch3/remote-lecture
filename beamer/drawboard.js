/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

function drawboardNewSlide(){
    document.getElementById("drawingArea").replaceChildren();
    var slide = document.getElementById("slide");
    Tools.SVGVirtualHeight = Tools.SVGVirtualWidth*
        slide.naturalHeight/slide.naturalWidth;
    Tools.svg.viewBox.baseVal.height = Tools.SVGVirtualHeight;
    socket.emit("getDrawboard", slideNum);
}

document.getElementById("slide").addEventListener("load", function (){
    drawboardNewSlide();
});

/* when this script is called, a slide might already be loaded */
drawboardNewSlide(); 

/* lecturer start */
Tools.change("Pencil");
Tools.setColor(Tools.colorPresets[1].color); // reddish

Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/5);
Tools.HTML.addShortcut(";", function(){ /* black pencil */
    if (Tools.curTool.name !== "Pencil"){
        Tools.change("Pencil");
        document.activeElement.blur && document.activeElement.blur();
    }
    Tools.setColor(Tools.colorPresets[0].color); // blackish
    Tools.setOpacity(1);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/12);
});

Tools.HTML.addShortcut(":", function(){ /* highlighter */
    if (Tools.curTool.name !== "Pencil"){
        Tools.change("Pencil");
        document.activeElement.blur && document.activeElement.blur();
    }
    Tools.setColor(Tools.colorPresets[4].color); // yellowish
    Tools.setOpacity(0.5);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/2);
});

Tools.HTML.addShortcut("!", function(){ /* red pointer */
    Tools.change("Pointer");
    document.activeElement.blur && document.activeElement.blur();
    Tools.setColor(Tools.colorPresets[1].color); // Reddish
    Tools.setOpacity(.8);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/5);
});
/* lecturer end */
