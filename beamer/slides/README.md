Put slides in this directory in SVG format, with names formatted as `slide_%d.svg`, where `%d` is the slide number, starting from 1. 

If slide numbers are not consecutive, gaps will be filled with blank slides with 4:3 aspect ratio.  
