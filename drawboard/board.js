/**
 *                        WHITEBOPHIR
 *********************************************************
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2013  Ophir LOJKINE; 2021  Hugo Raguet
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend
 * 
 * Modified by Hugo Raguet for Remote lecture, 2021
 */

var Tools = {}; // tools for drawboard, keep the variable name from WBO

Tools.SVGVirtualWidth = 10000; // beats any reasonable resolution
Tools.SVGVirtualHeight = 0; // will be adapted to actual slide aspect ratio

/* socket.io server defined in beamer/beamer.js */
Tools.socket = socket;

/* server_config_drawboard defined in drawboard/configuration.js */
Tools.server_config = server_config_drawboard;
/* MIN_TOOL_SIZE and MAX_TOOL_SIZE are relative to SVGVirtualWidth */
Tools.server_config.MIN_TOOL_SIZE *= Tools.SVGVirtualWidth;
Tools.server_config.MAX_TOOL_SIZE *= Tools.SVGVirtualWidth;

Tools.board = document.getElementById("board");
Tools.svg = document.getElementById("canvas");
Tools.drawingArea = Tools.svg.getElementById("drawingArea");

//Initialization
Tools.curTool = null;
Tools.showMarker = true;
Tools.showOtherCursors = true;
Tools.showMyCursor = true;

Tools.isIE = /MSIE|Trident/.test(window.navigator.userAgent);

Tools.list = {}; // An array of all known tools. {"toolName" : {toolObject}}

//Object containing the messages that have been received before the corresponding tool
//is loaded. keys : the name of the tool, values : array of messages for this tool
Tools.pendingMessages = {};

/**
 * Register a new tool, without touching the User Interface
 */
Tools.register = function registerTool(newTool) {
	if (newTool.name in Tools.list) {
		console.log("Tools.add: The tool '" + newTool.name + "' is already" +
			"in the list. Updating it...");
	}

	//Format the new tool correctly
	Tools.applyHooks(Tools.toolHooks, newTool);

	//Add the tool to the list
	Tools.list[newTool.name] = newTool;

	//There may be pending messages for the tool
	var pending = Tools.pendingMessages[newTool.name];
	if (pending) {
		console.log("Drawing pending messages for '%s'.", newTool.name);
		var msg;
		while (msg = pending.shift()) {
			//Transmit the message to the tool (precising that it comes from the network)
			newTool.draw(msg, false);
		}
	}
};

function srcPath(src) {
    return Tools.server_config.BASE_RESOURCE_PATH + src;
}

function urlPath(src) {
    return src.replace(/url\(['"](.+)['"]\)/,
        "url('" + Tools.server_config.BASE_RESOURCE_PATH + "$1')");
}

Tools.HTML = {
    /* lecturer start */
	template: new Minitpl("#tools > .tool"),
    
	addShortcut: function addShortcut(key, callback) {
		window.addEventListener("keydown", function (e) {
			if (e.key === key && !e.target.matches("input[type=text], textarea")) {
				callback();
			}
		});
	},

	addTool: function (toolName, toolIcon, toolIconHTML, toolShortcut, oneTouch) {
		var callback = function () {
			Tools.change(toolName);
		};
		this.addShortcut(toolShortcut, function () {
			Tools.change(toolName);
			document.activeElement.blur && document.activeElement.blur();
		});
		return this.template.add(function (elem) {
			elem.addEventListener("click", callback);
			elem.id = "toolID-" + toolName;
			elem.getElementsByClassName("tool-name")[0].textContent = toolName;
			var toolIconElem = elem.getElementsByClassName("tool-icon")[0];
			toolIconElem.src = srcPath(toolIcon);
			toolIconElem.alt = srcPath(toolIcon);
			if (oneTouch) elem.classList.add("oneTouch");
			elem.title =
				toolName + " (" +
				"raccourci clavier" + ": " +
				toolShortcut + ")" +
				(Tools.list[toolName].secondary ? " [" + "cliquer pour basculer" + "]" : "");
			if (Tools.list[toolName].secondary) {
				elem.classList.add('hasSecondary');
				var secondaryIcon = elem.getElementsByClassName('secondaryIcon')[0];
				secondaryIcon.src =
                    srcPath(Tools.list[toolName].secondary.icon);
				toolIconElem.classList.add("primaryIcon");
			}
		});
	},

	changeTool: function (oldToolName, newToolName) {
		var oldTool = document.getElementById("toolID-" + oldToolName);
		var newTool = document.getElementById("toolID-" + newToolName);
		if (oldTool) oldTool.classList.remove("curTool");
		if (newTool) newTool.classList.add("curTool");
	},

	toggle: function (toolName, name, icon) {
		var elem = document.getElementById("toolID-" + toolName);

		// Change secondary icon
		var primaryIcon = elem.getElementsByClassName("primaryIcon")[0];
		var secondaryIcon = elem.getElementsByClassName("secondaryIcon")[0];
		var primaryIconSrc = primaryIcon.src;
		var secondaryIconSrc = secondaryIcon.src;
		primaryIcon.src = srcPath(secondaryIconSrc);
		secondaryIcon.src = srcPath(primaryIconSrc);

		// Change primary icon
		elem.getElementsByClassName("tool-icon")[0].src = srcPath(icon);
		elem.getElementsByClassName("tool-name")[0].textContent = name;
	},

	colorPresetTemplate: new Minitpl("#colorPresetSel .colorPresetButton"),
	addColorButton: function (button) {
		var setColor = Tools.setColor.bind(Tools, button.color);
		if (button.key) this.addShortcut(button.key, setColor);
		return this.colorPresetTemplate.add(function (elem) {
			elem.addEventListener("click", setColor);
			elem.id = "color_" + button.color.replace(/^#/, '');
			elem.style.backgroundColor = button.color;
			if (button.key) {
				elem.title = "keyboard shortcut" + ": " + button.key;
			}
		});
	},

	colorPresetTemplateWhiteout: new Minitpl("#colorPresetSelWhiteout .colorPresetButton"),
	addColorButtonWhiteout: function (button) {
		var setColor = Tools.setColorWhiteout.bind(Tools, button.color);
		if (button.key) this.addShortcut(button.key, setColor);
		return this.colorPresetTemplateWhiteout.add(function (elem) {
			elem.addEventListener("click", setColor);
			elem.id = "color_" + button.color.replace(/^#/, '');
			elem.style.backgroundColor = button.color;
			if (button.key) {
				elem.title = "keyboard shortcut" + ": " + button.key;
			}
		});
	},
    /* lecturer end */

	addStylesheet: function (href) {
		//Adds a css stylesheet to the html or svg document
		var link = document.createElement("link");
		link.href = href;
		link.rel = "stylesheet";
		link.type = "text/css";
		document.head.appendChild(link);
	}
};

/**
 * Add a new tool to the user interface
 */
Tools.add = function (newTool) {
	Tools.register(newTool);

	if (newTool.stylesheet) {
		Tools.HTML.addStylesheet(srcPath(newTool.stylesheet));
	}

    /* lecturer start */
	//Add the tool to the GUI
	Tools.HTML.addTool(newTool.name, newTool.icon, newTool.iconHTML, newTool.shortcut, newTool.oneTouch);
    /* lecturer end */
};

/* lecturer start */
Tools.change = function (toolName) {
	var newTool = Tools.list[toolName];
	var oldTool = Tools.curTool;
	if (!newTool) throw new Error("Trying to select a tool that has never been added!");
	if (newTool === oldTool) {
		if (newTool.secondary) {
			newTool.secondary.active = !newTool.secondary.active;
			var props = newTool.secondary.active ? newTool.secondary : newTool;
			Tools.HTML.toggle(newTool.name, props.name, props.icon);
			if (newTool.secondary.switch) newTool.secondary.switch();
		}
		return;
	}
	if (!newTool.oneTouch) {
		//Update the GUI
		var curToolName = (Tools.curTool) ? Tools.curTool.name : "";
		try {
			Tools.HTML.changeTool(curToolName, toolName);
		} catch (e) {
			console.error("Unable to update the GUI with the new tool. " + e);
		}
		Tools.svg.style.cursor = urlPath(newTool.mouseCursor) || "auto";
		Tools.board.title = newTool.helpText || "";

		//There is not necessarily already a curTool
		if (Tools.curTool !== null) {
			//It's useless to do anything if the new tool is already selected
			if (newTool === Tools.curTool) return;

			//Remove the old event listeners
			Tools.removeToolListeners(Tools.curTool);

			//Call the callbacks of the old tool
			Tools.curTool.onquit(newTool);
		}

		//Add the new event listeners
		Tools.addToolListeners(newTool);
		Tools.curTool = newTool;
	}

	//Call the start callback of the new tool
	newTool.onstart(oldTool);
};

Tools.addToolListeners = function addToolListeners(tool) {
	for (var event in tool.compiledListeners) {
		var listener = tool.compiledListeners[event];
		var target = listener.target || Tools.board;
		target.addEventListener(event, listener, { 'passive': false });
	}
};

Tools.removeToolListeners = function removeToolListeners(tool) {
	for (var event in tool.compiledListeners) {
		var listener = tool.compiledListeners[event];
		var target = listener.target || Tools.board;
		target.removeEventListener(event, listener);
		// also attempt to remove with capture = true in IE
		if (Tools.isIE) target.removeEventListener(event, listener, true);
	}
};

(function () {
	// Handle secondary tool switch with shift (key code 16)
	function handleShift(active, evt) {
		if (evt.keyCode === 16 && Tools.curTool.secondary && Tools.curTool.secondary.active !== active) {
			Tools.change(Tools.curTool.name);
		}
	}
	window.addEventListener("keydown", handleShift.bind(null, true));
	window.addEventListener("keyup", handleShift.bind(null, false));
})();
/* lecturer end */

/* listener start */
Tools.addToolListeners = function addToolListeners(tool) {};
/* listener end */

// Send a message to the corresponding tool
function messageForTool(message) {
	var name = message.tool,
		tool = Tools.list[name];

	if (tool) {
		tool.draw(message, false);
	} else {
		///We received a message destinated to a tool that we don't have
		//So we add it to the pending messages
		if (!Tools.pendingMessages[name]) Tools.pendingMessages[name] = [message];
		else Tools.pendingMessages[name].push(message);
	}

	if (message.tool !== 'Hand' && message.deltax != null && message.deltay != null) {
		//this message has special info for the mover
		messageForTool({ tool: 'Hand', type: 'update', deltax: message.deltax || 0, deltay: message.deltay || 0, id: message.id });
	}
}

// Apply the function to all arguments by batches
function batchCall(fn, args) {
	var BATCH_SIZE = 1024;
	if (args.length === 0) {
		return Promise.resolve();
	} else {
		var batch = args.slice(0, BATCH_SIZE);
		var rest = args.slice(BATCH_SIZE);
		return Promise.all(batch.map(fn))
			.then(function () {
				return new Promise(requestAnimationFrame);
			}).then(batchCall.bind(null, fn, rest));
	}
}

// Call messageForTool recursively on the message and its children
function handleMessage(message) {
	//Check if the message is in the expected format
	if (!message.tool && !message._children) {
		console.error("Received a badly formatted message (no tool). ", message);
	}
	if (message.tool) { messageForTool(message); }
	if (message._children) return batchCall(handleMessage, message._children);
	else return Promise.resolve();
}

Tools.screenToSVGCoords = function screenToSVGCoords(X, Y) {
    var w = Tools.SVGVirtualWidth, h = Tools.SVGVirtualHeight,
        W = Tools.board.clientWidth, H = Tools.board.clientHeight,
        rw = w/W, rh = h/H;
    var SVGx, SVGy;
    if (rw < rh){ // canvas touches the board at top and bottom borders
        SVGx = (X - W/2)*rh + w/2;
        SVGy = Y*rh;
    }else{ // canvas touches the board at right and left borders
        SVGx = X*rw;
        SVGy = (Y - H/2)*rw + h/2;
    }
    return {x: SVGx, y: SVGy};
}

Tools.SVGToScreenCoords = function SVGToScreenCoords(x, y) {
    var w = Tools.SVGVirtualWidth, h = Tools.SVGVirtualHeight,
        W = Tools.board.clientWidth, H = Tools.board.clientHeight,
        rw = w/W, rh = h/H;
    var SVGx, SVGy;
    if (rw < rh){ // canvas touches the board at top and bottom borders
        X = (x - w/2)/rh + W/2;
        Y = y/rh;
    }else{ // canvas touches the board at right and left borders
        X = x/rw;
        Y = (y - h/2)/rw + H/2;
    }
    return {x: X, y: Y};
}


//List of hook functions that will be applied to tools before adding them
Tools.toolHooks = [
	function checkToolAttributes(tool) {
		if (typeof (tool.name) !== "string") throw "A tool must have a name";
		if (typeof (tool.listeners) !== "object") {
			tool.listeners = {};
		}
		if (typeof (tool.onstart) !== "function") {
			tool.onstart = function () { };
		}
		if (typeof (tool.onquit) !== "function") {
			tool.onquit = function () { };
		}
	},
    /* lecturer start */
	function compileListeners(tool) {
		//compile listeners into compiledListeners
		var listeners = tool.listeners;

		//A tool may provide precompiled listeners
		var compiled = tool.compiledListeners || {};
		tool.compiledListeners = compiled;

		function compile(listener) { //closure
			return (function listen(evt) {
                var coords = Tools.screenToSVGCoords(evt.pageX, evt.pageY);
				return listener(coords.x, coords.y, evt, false);
			});
		}

		function compileTouch(listener) { //closure
			return (function touchListen(evt) {
				//Currently, we don't handle multitouch
				if (evt.changedTouches.length === 1) {
					//evt.preventDefault();
					var touch = evt.changedTouches[0];
                    var coords = Tools.screenToSVGCoords(touch.pageX,
                        touch.pageY);
                    return listener(coords.x, coords.y, evt, true);
				}
				return true;
			});
		}

		function wrapUnsetHover(f, toolName) {
			return (function unsetHover(evt) {
				document.activeElement && document.activeElement.blur && document.activeElement.blur();
				return f(evt);
			});
		}

		if (listeners.press) {
			compiled["mousedown"] = wrapUnsetHover(compile(listeners.press), tool.name);
			compiled["touchstart"] = wrapUnsetHover(compileTouch(listeners.press), tool.name);
		}
		if (listeners.move) {
			compiled["mousemove"] = compile(listeners.move);
			compiled["touchmove"] = compileTouch(listeners.move);
		}
		if (listeners.release) {
			var release = compile(listeners.release),
				releaseTouch = compileTouch(listeners.release);
			compiled["mouseup"] = release;
			if (!Tools.isIE) compiled["mouseleave"] = release;
			compiled["touchleave"] = releaseTouch;
			compiled["touchend"] = releaseTouch;
			compiled["touchcancel"] = releaseTouch;
		}
	}
    /* lecturer end */
];

Tools.applyHooks = function (hooks, object) {
	//Apply every hooks on the object
	hooks.forEach(function (hook) {
		hook(object);
	});
};

// Utility functions

/* lecturer start */
Tools.generateUID = function (prefix, suffix) {
	var uid = Date.now().toString(36); //Create the uids in chronological order
	uid += (Math.round(Math.random() * 36)).toString(36); //Add a random character at the end
	if (prefix) uid = prefix + uid;
	if (suffix) uid = uid + suffix;
	return uid;
};
/* lecturer end */

Tools.createSVGElement = function createSVGElement(name, attrs) {
	var elem = document.createElementNS(Tools.svg.namespaceURI, name);
	if (typeof (attrs) !== "object") return elem;
	Object.keys(attrs).forEach(function (key, i) {
		elem.setAttributeNS(null, key, attrs[key]);
	});
	return elem;
};

/* lecturer start */
Tools.colorPresets = [
	{ color: "#001f3f", key: '1' },
	{ color: "#FF4136", key: '2' },
	{ color: "#0074D9", key: '3' },
	{ color: "#FF851B", key: '4' },
	{ color: "#FFDC00", key: '5' },
	{ color: "#3D9970", key: '6' },
	{ color: "#91E99B", key: '7' },
	{ color: "#90468b", key: '8' },
	{ color: "#7FDBFF", key: '9' },
	{ color: "#AAAAAA", key: '0' },
	{ color: "#E65194" },
	{ color: "#FFFFFF"}
];

Tools.colorPresetsWhiteout = [
	{ color: "#FFFFFF"},
	{ color: "#FFDC00"},
	{ color: "#FF851B"},
	{ color: "#0074D9" },
	{ color: "#FF4136"},
	{ color: "#91E99B"},
	{ color: "#3D9970"},
	{ color: "#AAAAAA"},
	{ color: "#E65194"},
	{ color: "#90468b"},
	{ color: "#7FDBFF"},
	{ color: "#001f3f"}
]
/* lecturer end */

/* tool color */
Tools.color_chooser = document.getElementById("chooseColor");
Tools.setColor = function (color) {
	Tools.color_chooser.value = color;
};
Tools.getColor = (function color() {
    var initial_color = "#001f3f";
	Tools.setColor(initial_color);
	return function () { return Tools.color_chooser.value; };
})();

/* whiteout color */
Tools.white_color_chooser = document.getElementById("chooseColorWhiteout");
Tools.setColorWhiteout = function (color) {
	Tools.white_color_chooser.value = color;
};
Tools.getColorWhiteout = (function color(){
	var initial_color = "#FFFFFF";
	Tools.setColorWhiteout(initial_color);
	return function () { return Tools.white_color_chooser.value; };
})();

/* lecturer start */
Tools.colorPresets.forEach(Tools.HTML.addColorButton.bind(Tools.HTML));
Tools.colorPresetsWhiteout.forEach(Tools.HTML.addColorButtonWhiteout.bind(Tools.HTML));

/* lecturer end */

Tools.setSize = (function size() {
	var chooser = document.getElementById("chooseSize");
    chooser.min = Tools.server_config.MIN_TOOL_SIZE;
    chooser.max = Tools.server_config.MAX_TOOL_SIZE;
    chooser.step = Tools.server_config.MAX_TOOL_SIZE/50;

	function update() {
		var size = Math.max(Tools.server_config.MIN_TOOL_SIZE,
            Math.min(Tools.server_config.MAX_TOOL_SIZE, chooser.value | 0));
		chooser.value = size;
	}
	update();

	chooser.onchange = chooser.oninput = update;
	return function (value) {
		if (value !== null && value !== undefined) {
            chooser.value = value;
            update();
        }
		return Number(chooser.value);
	};
})();

Tools.getSize = (function () { return Tools.setSize() });

Tools.setOpacity = (function opacity() {
	var chooser = document.getElementById("chooseOpacity");
	var opacityIndicator = document.getElementById("opacityIndicator");

	function update() {
        var opacity = Math.max(0.1, Math.min(1, chooser.value || 0));
        chooser.value = opacity;
		opacityIndicator.setAttribute("opacity", chooser.value);
	}
	update();

	chooser.onchange = chooser.oninput = update;
	return function (value) {
		if (value !== null && value !== undefined) {
            chooser.value = value;
            update();
        }
		return chooser.value;
	};
})();

Tools.getOpacity = (function () { return Tools.setOpacity() });

Tools.socket.on("broadcastDraw", function (msg) { handleMessage(msg); });

/* lecturer start */
Tools.send = function (data, toolName) {
	toolName = toolName || Tools.curTool.name;
	var d = data;
	d.tool = toolName;
	var message = { "data": d };
	Tools.socket.emit('broadcastDraw', message);
};
/* lecturer end */

Tools.drawAndSend = function (data, tool) {
	if (tool == null) tool = Tools.curTool;
	tool.draw(data);
    /* lecturer start */
	Tools.send(data, tool.name);
    /* lecturer end */
}
