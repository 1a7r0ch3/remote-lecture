/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const path = require('path');
const drawboard = require('./drawboard.js');
const config = require('./configuration.js');
const child_proc = require("child_process");
const os = require('os');

const SLIDES_DIR = path.join("beamer", "slides");
const UPLOAD_DIR = path.join("beamer", "upload");

for (const dir of [SLIDES_DIR, UPLOAD_DIR]){
    if (!fs.existsSync(dir)){
        fs.mkdir(dir, (err) => { if (err) throw err; });
    }
}

/* current slide show */
var slideshow, /* map slide number to cached slides; first always 0 */
    slides, /* cached slides content; first always empty */
    slideshowNum, /* slideshow identifier for cache mechanism */
    slideNum, /* current slide */
    prevSlideNum, /* used get back to previous slide after a jump */
    numberOfSlides,
    backgroundColor = config.beamer.BACKGROUND_COLOR;

function reinitializeSlideshow(){
    if (slideshowNum === undefined){ slideshowNum = 0; }
    else{ slideshowNum += 1; }
    slideshow = [0];
    slides = [emptySlide()];
    slideNum = 0;
    prevSlideNum = 0;
    numberOfSlides = 0;
    drawboard.removeAllBoards();
}

/* empty slide (4:3 aspect ratio) */
function emptySlide(){
    return '<svg width="4" height="3" viewBox="0 0 4 3" ' + 
        'xmlns="http://www.w3.org/2000/svg" ' + 
        'xmlns:xlink="http://www.w3.org/1999/xlink">' +
    '</svg>';
}

/* blank slide (4:3 aspect ratio) */
function blankSlide(){
    return '<svg width="4" height="3" viewBox="0 0 4 3" ' + 
        'xmlns="http://www.w3.org/2000/svg" ' + 
        'xmlns:xlink="http://www.w3.org/1999/xlink">' +
        '<rect width="4" height="3" style="fill:white; stroke-width:0"/>' +
    '</svg>';
}

/* add slides and empty drawboards accordingly */
async function putSlidesSync(slideArray){
    let n = slides.length;
    numberOfSlides += slideArray.length;
    incrSlide(1);
    await Promise.all([
        slideshow.splice(slideNum, 0,
            ...Array(slideArray.length).fill().map((_, i) => n + i)),
        slides.push(...slideArray),
        drawboard.insertBoards(slideNum, slideArray.length)
    ]);
}

function getSlidesFromDirSync(dir, number){
    return Array(number).fill().map((_, i) =>
        getSlideFromFileSync(path.join(dir, "slide_" + (i + 1) + ".svg"))
    );
}

function getSlideFromFileSync(file){
    if (!fs.existsSync(file)){ return blankSlide(); }
    let content = fs.readFileSync(file, "UTF-8");
    /* check for viewBox property, needed for fitting to client screen */
    if (!content.includes("viewBox")){
        let insertIndex = content.indexOf('svg') + 4;
        /* get height */
        let heightIndex = content.indexOf('height');
        let height = content.substr(heightIndex + 8).match(/\d+\.?\d*/);
        /* get width */
        let widthIndex = content.indexOf('width');
        let width = content.substr(widthIndex + 7).match(/\d+\.?\d*/);
        /* insert viewbox */
        content = content.slice(0, insertIndex) + 
            ' viewBox="0 0 ' + width + ' ' +  height + '" '  + 
            content.slice(insertIndex);
    }
    return content;
}

/* delete slide and corresponding drawboard */
async function cutSlideSync(){
    if (slideNum === 0){ /* no slides; do nothing */ return; }
    await Promise.all([
        slideshow.splice(slideNum, 1),
        drawboard.removeBoard(slideNum)
    ]);
    numberOfSlides -= 1;
    if (numberOfSlides === 0){ reinitializeSlideshow(); }
    else if (slideNum > numberOfSlides){ slideNum = numberOfSlides; }
}

/***  navigation mechanism  ***/

function incrSlide(n){
    prevSlideNum = slideNum;
    slideNum = slideNum + n;
    if (slideNum < 1){ slideNum = 1; }
    if (slideNum > numberOfSlides){ slideNum = numberOfSlides; }
}

function lastSlide(){
    prevSlideNum = slideNum;
    slideNum = numberOfSlides;
}

function prevSlide(){
    slideNum = prevSlideNum;
}

/***  construct first slideshow from SLIDES_DIR  ***/

numberOfSlideFiles = Math.max(0, ...
    fs.readdirSync(SLIDES_DIR)
    .filter(fname => fname.match(/slide_(\d+)\.svg/))
    .map(fname => Number(fname.split('_')[1].split('.')[0])));

reinitializeSlideshow();
putSlidesSync(getSlidesFromDirSync(SLIDES_DIR, numberOfSlideFiles));

/***  uploading mecanism  ***/

var uploadingFileName = null,
    uploadingFileType = null,
    uploadingSocket = null,
    hasPdf2Svg = existCmd("pdf2svg"),
    hasSvgCleaner = existCmd("svgcleaner");
    hasConvert = existCmd("convert");

function existCmd(cmd){
    let wh = os.platform() === "win32" ? "where" : "which";
    let exist = true;
    try{ /* execSync throws on nonzero exit code */
        child_proc.execSync(wh + " " + cmd, { stdio: 'ignore' });
    }catch (err){
        exist = false;
    }
    return exist;
}

async function processUploadFile() {
    if (uploadingFileType === 'application/pdf'){
        /* convert PDF to SVG */
        let cmd = "cd " + UPLOAD_DIR + " && " + 
            "pdf2svg " +  uploadingFileName + ' "slide_%d.svg" '  + "all";
        try{ /* execSync throws on nonzero exit code */
            await child_proc.execSync(cmd, { stdio: 'ignore' });
        }catch (err){
            console.log("Beamer: pdf2svg failed with: " + err.message);
            uploadingSocket.emit("stopUploadSlide",
                "La conversion depuis le format PDF a échoué ; essayez"
                + " le format SVG ou contactez l'administrateur.");
            resetUploadSlides();
            return;
        }
        if (hasSvgCleaner){ /* compress resulting SVG files */
            let cmd = "cd " + UPLOAD_DIR + " && " +
                "for file in *.svg; do svgcleaner $file $file; done";
            try{ /* execSync throws on nonzero exit code */
                await child_proc.execSync(cmd, { stdio: 'ignore' });
            }catch (err){
                console.log("Beamer: svgcleaner failed with: " + err.message);
            }
        }
        let numberOfUploadSlides = fs.readdirSync(UPLOAD_DIR).
            filter(fname => fname.match(/slide_(\d+)\.svg/)).length;
        await putSlidesSync(getSlidesFromDirSync(UPLOAD_DIR,
            numberOfUploadSlides));
    }else if (uploadingFileType === 'image/svg+xml'){
        await putSlidesSync([getSlideFromFileSync(path.join(UPLOAD_DIR,
            uploadingFileName))]);
    }else{ /* image */
        fullFileName = path.join(UPLOAD_DIR, uploadingFileName);
        let width, height;
        let cmd = "convert " + fullFileName + " -print '%w %h' /dev/null";
        try{ /* execSync throws on nonzero exit code */
            let wh = await child_proc.execSync(cmd);
            width = wh.toString().split(" ")[0];
            height = wh.toString().split(" ")[1];
        }catch (err){
            console.log("Beamer: convert failed with: " + err.message);
            uploadingSocket.emit("stopUploadSlide",
                "La conversion depuis le format image a échoué ; essayez"
                + " le format SVG ou contactez l'administrateur.");
            resetUploadSlides();
            return;
        }
        let slide = '<svg width="' + width + '" height="' + height + '" ' +
            'viewBox="0 0 ' + width + ' ' + height + '" ' +
            'xmlns="http://www.w3.org/2000/svg" ' +
            'xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
            '<image xlink:href="data:' + uploadingFileType + ';base64,' +
            fs.readFileSync(fullFileName).base64Slice() + '" />\n</svg>';
        putSlidesSync([slide]);
    }

    uploadingSocket.emit("endUploadSlide");

    broadcastSlideshow(uploadingSocket);

    resetUploadSlides();
}

function broadcastSlideshow(socket){
    let slideshowInfo = {
        "slideshow": slideshow,
        "slideshowNum": slideshowNum,
        "slideNum": slideNum
    };
    socket.broadcast.emit("newSlideshow", slideshowInfo);
    socket.emit("newSlideshow", slideshowInfo);
}

function resetUploadSlides(){
    fs.readdir(UPLOAD_DIR, function(err, files){
        if (err){ console.log("Beamer: " + err.message); }
        for (const file of files) {
            fs.unlink(path.join(UPLOAD_DIR, file), function(err){
                if (err){ console.log("Beamer " + err.message); }
            });
        }
    });
    uploadingFileName = null;
    uploadingFileType = null;
    uploadingSocket = null;
}

function uploadFileRequest(request, response){
    let fileChunks = [];
    let fileSize = 0; 
    request.on('data', function(chunk){
        /* sanity check: an upload was indeed started */
        if (uploadingSocket === null){
            console.log("Beamer: POST request at " + request.url + ": " +
                "uploading variables not set yet; something is wrong.");
            resetUploadSlides();
            return;
        }
        /* sanity check: file size does not exceed limit */
        fileSize += chunk.length;
        if (fileSize > config.beamer.MAX_UPLOAD_SIZE){
            console.log("Beamer: POST request at" + request.url + ": " +
                "trying to upload file larger than" +
                (config.beamer.MAX_UPLOAD_SIZE / (1024*1024)) + "MiB");
            resetUploadSlides();
            return;
        }
        fileChunks.push(chunk);
    });
    request.on('end', function(){
        let content = Buffer.concat(fileChunks);
        fs.writeFile(path.join(UPLOAD_DIR, uploadingFileName), content,
            function(err){
                if (err){
                    console.log("Beamer: " + err.message);
                    uploadingSocket.emit("stopUploadSlide",
                        "L'écriture du fichier a échoué ; contactez " +
                        "l'administrateur du serveur.");
                    resetUploadSlides();
                }
                processUploadFile();
            }
        );
        response.writeHead(202);
        response.end();     
    });
}

/* serve beamer slides; return true if resource is found, false otherwise */
var slideRegExp = new RegExp(path.join(SLIDES_DIR,
    "slide_(\\d+)_(\\d+)\\.svg"));

function serve(url, response) {
    let match = url.match(slideRegExp);
    if (match){
        response.writeHead(200, {
            "Content-Type": "image/svg+xml",
            "Cache-control": "max-age=" + config.beamer.CACHE_MAX_AGE +
                ", must-revalidate"
        });
        response.end(slides[Number(match[2])]);
        return true;
    }else{
        return false;
    }
}

/***  communications throught sockets  ***/ 

/* broadcasting slide number */
function goToSlide(socket){
    socket.broadcast.emit("goToSlide", slideNum);
    socket.emit("goToSlide", slideNum);
}

function onConnection(socket){
    if (socket.handshake.auth.token === config.LECTURER_KEY){
        socket.on("incrSlide", function(n){
            incrSlide(n);
            goToSlide(socket);
        });

        socket.on("lastSlide", function(){
            lastSlide();
            goToSlide(socket);
        });

        socket.on("prevSlide", function(){
            prevSlide();
            goToSlide(socket);
        });

        socket.on("addBlankSlide", async function(){
            await putSlidesSync([blankSlide()]);
            broadcastSlideshow(socket);
        });

        socket.on("startUploadSlide", function(fileInfo){
            if (uploadingSocket){ /* already uploading */
                socket.emit("stopUploadSlide", "Un fichier est déjà en " +
                    "cours de téléversement.");
                return;
            }
            if (fileInfo.type === "application/pdf" && !hasPdf2Svg){
                socket.emit("stopUploadSlide", "Actuellement, le serveur ne " +
                    "supporte pas le format PDF; l'administrateur doit " +
                    "installer pdf2svg.");
                return;
            }else if (fileInfo.type !== "image/svg+xml" && !hasConvert){ 
                socket.emit("stopUploadSlide", "Actuellement, le seul format" +
                    " d'image que le serveur supporte est SVG; pour " +
                    "supporter d'autres formats d'image, l'administrateur " +
                    "doit installer convert.");
                return;
            }
            /* all is ok; set uploading variables and send startUploadSlide */
            uploadingSocket = socket;
            uploadingFileName = fileInfo.filename;
            uploadingFileType = fileInfo.type;
            socket.emit("startUploadSlide");
        });

        socket.on("cleanSlide", function(){
            drawboard.cleanBoard(slideNum);
            goToSlide(socket);
        });

        socket.on("cleanAllSlides", function(){
            drawboard.cleanAllBoards();
            goToSlide(socket);
        });

        socket.on("cutSlide", async function(){
            await cutSlideSync();
            broadcastSlideshow(socket);
        });

        socket.on("cutAllSlides", async function(){
            reinitializeSlideshow();
            broadcastSlideshow(socket);
        });

        socket.on("backgroundColor", function(color){
            backgroundColor = color;
            socket.broadcast.emit("backgroundColor", color);
            socket.emit("backgroundColor", color);
        });
        
        /* broadcasting drawboard messages */
        socket.on("broadcastDraw", function (msg) {
            drawboard.handleMessage(slideNum, msg.data, socket);
            socket.broadcast.emit("broadcastDraw", msg.data);
        });
    }
    
    socket.on("getDrawboard", async function (name) {
        let board = await drawboard.getBoard(name);
        socket.emit("broadcastDraw", { _children: board.getAll() });
    });

    /* start beamer with current slideshow */
    socket.emit("newSlideshow", {
        "slideshow": slideshow,
        "slideshowNum": slideshowNum,
        "slideNum": slideNum
    });
    socket.emit("backgroundColor", backgroundColor);
}

const info = "Beamer slides are in\n" +
    "  " + path.join(path.resolve("."), SLIDES_DIR) + 
    ("\n(" + (numberOfSlides ? numberOfSlides : "no") + " slide" +
     (numberOfSlides > 1 ? "s" : "") + " found)") +
    (hasPdf2Svg ? "" :
     "\n\n`pdf2svg` not available on the system: clients will not be able to" +
     " upload slides from PDF sources.") +
    (!hasPdf2Svg || hasSvgCleaner ? "" : 
     "\n\n`svgcleaner` not available on the system: install svgcleaner for " +
     "reducing sizes of slides uploaded by clients from PDF sources.") +
    (hasConvert ? "" :
     "\n\n`convert` not available on the system: clients will not be able to" +
     " upload slides from other image formats than SVG.");

module.exports = { serve, uploadFileRequest, onConnection, info };
