/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const BoardData = require("./boardData.js").BoardData;
const config = require('./configuration.js');
const fs = require('fs');
const path = require('path');

/** Array of *promises* of BoardData
	@type {Object<string, Promise<BoardData>>}
*/
var boards = [];

function getBoard(boardNum){
    if (!boards[boardNum]){
        boards[boardNum] = new BoardData("slide_" + boardNum);
    }
    return boards[boardNum];
}

async function insertBoards(boardNum, number){
    await boards.splice(boardNum, 0, ...Array(number).fill(null));
}

function cleanBoard(boardNum){
    delete boards[boardNum];
}

function cleanAllBoards(){
    for (num in boards){ delete boards[num]; }
}

function removeBoard(boardNum){
    boards.splice(boardNum, 1);
}

function removeAllBoards(){
    boards = [];
}

function handleMessage(boardNum, message, socket){
    if (message.tool === "Cursor"){
        message.socket = socket.id;
    }else if (message.tool !== "Pointer"){
        saveHistory(boardNum, message);
    }
}

async function saveHistory(boardNum, message) {
    var id = message.id;
    var board = await getBoard(boardNum);
    switch (message.type) {
    case "delete":
        if (id) board.delete(id);
        break;
    case "update":
        if (id) board.update(id, message);
        break;
    case "child":
    case "endline":
        board.addChild(message.parent, message);
        break;
    default:
        //Add data
        if (!id) throw new Error("Invalid message: ", message);
        board.set(id, message);
    }
}

/* export client-side configuration */
fs.writeFile("drawboard/configuration.js",
    "const server_config_drawboard = " + JSON.stringify({
        MAX_EMIT_COUNT: config.drawboard.MAX_EMIT_COUNT,
        MAX_EMIT_COUNT_PERIOD: config.drawboard.MAX_EMIT_COUNT_PERIOD,
        AUTO_FINGER_WHITEOUT: config.drawboard.AUTO_FINGER_WHITEOUT,
        BASE_RESOURCE_PATH: config.drawboard.BASE_RESOURCE_PATH,
        POINTER_TIMEOUT_MS: config.drawboard.POINTER_TIMEOUT_MS,
        CURSOR_DELETE_AFTER_MS: config.drawboard.CURSOR_DELETE_AFTER_MS,
        MIN_TOOL_SIZE: config.drawboard.MIN_TOOL_SIZE,
        MAX_TOOL_SIZE: config.drawboard.MAX_TOOL_SIZE
    }) + ";",
    function (err) { if (err) console.log(err); }
);

module.exports = { handleMessage, getBoard, insertBoards, cleanBoard,
    cleanAllBoards, removeBoard, removeAllBoards };
