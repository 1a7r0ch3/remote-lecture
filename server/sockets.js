/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const config = require('./configuration.js');
const beamer = require('./beamer.js');
const drawboard = require('./drawboard.js');
const textboard = require('./textboard.js');
const terminal = require('./terminal.js');
const fs = require("fs");
const iolib = require("socket.io");

var media = "home",
    io,
    numberOfListeners = 0,
    numberOfLecturers = 0,
    peakOfConnexions = 0;

const CONN_STAT_TIMEOUT_MS = 1000;
var writeTimeoutId;

function writeAndSendConnectionStats() {
    if (writeTimeoutId){ clearTimeout(writeTimeoutId); }
    writeTimeoutId = setTimeout(function () {
        fs.writeFile(config.CONNECTION_STATS_FILE,
            "current lecturers: " + numberOfLecturers + "\n" + 
            "current listeners: " + numberOfListeners + "\n" +
            "connexions peak: " + peakOfConnexions + "\n",
            function(err) { if(err) { return console.log(err); } }
        );
        io.emit("connectionStats", {
            lecturers: numberOfLecturers,
            listeners: numberOfListeners,
            peak: peakOfConnexions
        });
    }, CONN_STAT_TIMEOUT_MS);
}

function onConnection(socket) {

    switch (media) {

    case "home":
        if (socket.handshake.auth.token === config.LECTURER_KEY){
            /* broadcasting media */
            socket.on("goToBeamer", function() {
                media = "beamer";
                textboard.stop();
                terminal.stop();
                io.emit("goToBeamer");
            });

            socket.on("goToTextboard", function() {
                media = "textboard";
                textboard.start(io);
                terminal.stop();
                io.emit("goToTextboard");
            });

            socket.on("goToTerminal", function() {
                media = "terminal";
                textboard.stop();
                terminal.start(io);
                io.emit("goToTerminal");
            });
        }
        break;

    case "beamer":
        socket.emit("goToBeamer");
        beamer.onConnection(socket);
        break;

    case "textboard":
        socket.emit("goToTextboard");
        textboard.onConnection(socket);
        break;
        
    case "terminal":
        socket.emit("goToTerminal");
        terminal.onConnection(socket);
        break;
    }

    if (media !== "home" &&
        socket.handshake.auth.token === config.LECTURER_KEY){
        /* back to home */
        socket.on("goHome", function() {
            media = "home";
            textboard.stop();
            terminal.stop();
            io.emit("goHome");
        });
    }

    /* count and peak connections */
    if (socket.handshake.auth.token === config.LECTURER_KEY){
        numberOfLecturers += 1;
    }else{
        numberOfListeners += 1;
    }
    if (numberOfLecturers + numberOfListeners > peakOfConnexions){
        peakOfConnexions = numberOfLecturers + numberOfListeners;
    }
    writeAndSendConnectionStats(socket);

    socket.on("disconnect", function(){
        if (socket.handshake.auth.token === config.LECTURER_KEY){
            numberOfLecturers -= 1;
        }else{
            numberOfListeners -= 1;
        }
        writeAndSendConnectionStats(socket);
    });

}

function start(server) {
    io = iolib(server);
    io.sockets.on("connection", onConnection);
}

module.exports = { start };
