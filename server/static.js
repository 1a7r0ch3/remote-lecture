/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const config = require('./configuration.js');

/* list of static resources that must be preprocessed */
const preprocFileList = [
    "common/home.html",
    "common/userInterface.js",
    "beamer/beamer.html",
    "beamer/beamer.js",
    "beamer/drawboard.js",
    "drawboard/board.js",
    "terminal/terminal.html",
    "terminal/terminal.js",
    "textboard/textboard.html",
    "textboard/textboard.js"
];

/* list of simple static resources */
const staticFileList = [
    "common/remote-lecture.css",
    "beamer/beamer.css",
    "beamer/assets/blank_slide.svg",
    "beamer/assets/filled_slides.svg",
    "beamer/assets/clean_slide.svg",
    "beamer/assets/clean_all_slides.svg",
    "beamer/assets/cut_slide.svg",
    "beamer/assets/cut_all_slides.svg",
    "beamer/configuration.js",
    "drawboard/board.css",
    "drawboard/path-data-polyfill.js",
    "drawboard/minitpl.js",
    "drawboard/configuration.js",
    "drawboard/tools/cursor/cursor.js",
    "drawboard/tools/ellipse/ellipse.js",
    "drawboard/tools/ellipse/ellipse.css",
    "drawboard/tools/ellipse/icon-circle.svg",
    "drawboard/tools/ellipse/icon-ellipse.svg",
    "drawboard/tools/eraser/eraser.js",
    "drawboard/tools/eraser/icon.svg",
    "drawboard/tools/grid/grid.js",
    "drawboard/tools/grid/icon.svg",
    "drawboard/tools/line/line.js",
    "drawboard/tools/line/line.css",
    "drawboard/tools/line/icon.svg",
    "drawboard/tools/line/icon-straight.svg",
    "drawboard/tools/pencil/wbo_pencil_point.js",
    "drawboard/tools/pencil/pencil.js",
    "drawboard/tools/pencil/pencil.css",
    "drawboard/tools/pencil/icon.svg",
    "drawboard/tools/pencil/cursor.svg",
    "drawboard/tools/pencil/laser_icon.svg",
    "drawboard/tools/pencil/laser_cursor.svg",
    "drawboard/tools/pencil/whiteout_tape.svg",
    "drawboard/tools/rect/rect.js",
    "drawboard/tools/rect/rect.css",
    "drawboard/tools/rect/icon.svg",
    "drawboard/tools/rect/icon-square.svg",
    "drawboard/tools/rect/icon.svg",
    "drawboard/tools/text/text.js",
    "drawboard/tools/text/text.css",
    "drawboard/tools/text/icon.svg",
    "drawboard/tools/icon-size.svg",
    "terminal/terminal.css",
    "terminal/xterm/xterm.css",
    "terminal/xterm/xterm.js",
    "textboard/textboard.css",
    "textboard/jqmath/jquery-1.4.3.js",
    "textboard/jqmath/jqmath-0.4.6.js",
    "textboard/jqmath/jscurry-0.4.5.js",
    "textboard/jqmath/jqmath-0.4.3.css",
    "textboard/prism/prism-core.min.js",
    "textboard/prism/prism-normalize-whitespace.min.js",
    "textboard/prism/prism-clike.js",
    "textboard/prism/prism-c-textboard.js",
    "textboard/prism/prism-pseudocode.js",
    "textboard/prism/prism-textboard.css"
];

/* preload and preprocess scripts and HTML resources */
var preprocLecturerFiles = {};
var preprocListenerFiles = {};
var head = fs.readFileSync("common/head.html", "UTF-8");
for (const file of preprocFileList){
    let content = fs.readFileSync(file, "UTF-8");
    content = content.replace(/<!-- head -->/g, head);
    content = content.replace(/<!-- title -->/g, config.TITLE);
    var blockRegExp;
    /* create version for listener: remove lecturer parts */
    blockRegExp = new RegExp([ // split regexp over multiple lines
        /(<!--|\/\*) lecturer start (\*\/|-->)/,
        /(.|\n)*?/,
        /(<!--|\/\*) lecturer end (\*\/|-->)/
    ].map(function(r) {return r.source}).join(''), "g");
    preprocListenerFiles[file] = content.replace(blockRegExp, "");
    /* create version for lecturer: remove listener parts and add key */
    content = content.replace(/@lecturer-key/g, config.LECTURER_KEY);
    blockRegExp = new RegExp([ // split regexp over multiple lines
        /(<!--|\/\*) listener start (\*\/|-->)/,
        /(.|\n)*?/,
        /(<!--|\/\*) listener end (\*\/|-->)/
    ].map(function(r) {return r.source}).join(''), "g");
    preprocLecturerFiles[file] = content.replace(blockRegExp, "");
}

/* automatic header deduction */
function deduceHeader(filename){
    if (/\.html$/.test(filename)){
        return {"Content-Type": "text/html; charset=utf-8"};
    }else if (/\.js$/.test(filename)){
        return {"Content-Type": "text/javascript"};
    }else if (/\.css$/.test(filename)){
        return {"Content-Type": "text/css"};
    }else if (/\.svg$/.test(filename)){
        return {"Content-Type": "image/svg+xml"};
    }
    return null;
}

/* look for str in any string in the list;
 * return the first occurrence in the list, if any */
function matchInList(str, list){
    for (const file of list){ if (str.match(file)){ return file; } }
    return null;
}

/* serve static files depending on url and lecturer status
 * return true if resource is found, false otherwise */
function serve(url, response, isLecturer){
    if (url === "/" ||
        url === "/" + config.LECTURER_KEY ||
        url === "/" + config.LECTURER_KEY + "/"){
        response.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
        response.end(isLecturer ? preprocLecturerFiles["common/home.html"]
                                : preprocListenerFiles["common/home.html"]);
        return true;
    }

    var match;
    
    if (match = matchInList(url, preprocFileList)){
        response.writeHead(200, deduceHeader(match));
        response.end(isLecturer ? preprocLecturerFiles[match]
                                : preprocListenerFiles[match]);
        return true;
    }

    if (match = matchInList(url, staticFileList)){
        fs.readFile(match, "UTF-8", function(err, fileContent) {
            if (err) throw err;
            response.writeHead(200, deduceHeader(match));
            response.end(fileContent);
        });
        return true;
    }
    
    return false;
}

module.exports = { serve };
