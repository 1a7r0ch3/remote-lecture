/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const path = require('path');
const BOARDS = ["textboard/textboard0.tbd", "textboard/textboard1.tbd"];

var watchers = [null, null];

function update(i, socket){
    fs.readFile(BOARDS[i], "UTF-8", function(err, content) {
        if (err) throw err;
        socket.emit("updateTextboard" + i, content);
    });
}

function start(socket) {
    for (let i = 0; i < BOARDS.length; i++){
        watchers[i] = fs.watch(BOARDS[i], function(event, filename) {
            if (event == "change") { update(i, socket); }
        });
    }
}

function stop() {
    for (let i = 0; i < BOARDS.length; i++){
        if (watchers[i] !== null){
            watchers[i].close();
            watchers[i] = null;
        }
    }
}

function onConnection(socket) {
    for (let i = 0; i < BOARDS.length; i++){ update(i, socket); }
}

const info = "Broadcast textboard by writing on\n" +
    BOARDS.map((name) => "  " + path.join(path.resolve("."), name)).join("\n");

module.exports = { start, stop, onConnection, info };
