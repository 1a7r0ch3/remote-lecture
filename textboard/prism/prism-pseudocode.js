Prism.languages.pseudo = {
    'comment': {
		pattern: /(^|[^\\])#.*/,
		lookbehind: true
	},
	'string': {
		pattern: /“.*”/,
		greedy: true
	},
	'char': {
		pattern: /‘.’/,
		greedy: true
	},
	'function': /[a-z_]\w*(?=\s*\()/i,
	'number': /(?:\b\d+(?:,\d*)?)/,
	'operator': /[+-=≠←→≥≤></\\÷⨯—]/,
    'empty': /∅/,
    'boolean': /\b(?:vrai|faux)\b/,
	'keyword': /\b(?:réf|et|ou|non|Si|alors|sinon si|sinon|répéter|Répéter|Algorithme|selon|exception|terminer|Tant que|tant que|Pour|pour|parcourant|nélém|,|\(|\)|\.|\:)\b/
};
